package de.thws.karten;

public  enum Suit
{
  CLUBS, SPADES, HEARTS, DIAMONDS
};