package de.thws.karten;

public enum Rank
{
    SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE
}
