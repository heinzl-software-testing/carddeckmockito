package de.thws.karten;

import java.util.ArrayList;
import java.util.List;

public class Game
{
  public static final int NUMBER_OF_CARDS_FOR_EACH_PLAYERS_STARTING_HAND = 5;
  Deck deck;
  List<Player> players = new ArrayList<>();
  boolean running = false;

  public Game()
  {
    deck = new Deck();
  }

  Game(Deck deck, Player... players)
  {
    this.deck = deck;
    this.players.addAll(List.of(players));
  }

  /**
   * prepares the game and starts it
   * @return the modified game object for fluent access
   */
  public Game createGame()
  {
    deck.reset();
    if (players.size() == 0) throw new RuntimeException("Fewer than 1 player registered.");
    players.forEach(s -> s.drawStartingHand(deck));
    running = true;
    return this;
  }

  public Game addPlayer(Player player)
  {
    players.add(player);
    return this;
  }
}
