package de.thws.karten;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class Player
{
  List<Card> hand = new ArrayList<>();

  public void drawStartingHand(Deck deck)
  {
    hand = IntStream.range(0, Game.NUMBER_OF_CARDS_FOR_EACH_PLAYERS_STARTING_HAND)
        .mapToObj(i -> deck.removeTopmostCard())
      .toList();
  }
}
