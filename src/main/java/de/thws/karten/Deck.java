package de.thws.karten;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

public class Deck
{
  List<Card> cards = new ArrayList<>();

  public void reset()
  {
    List<Card> deck = Stream.of(Suit.values())
        .map(f -> Stream.of(Rank.values())
            .map(w -> new Card(f, w)))
        .flatMap(l -> l)
        .toList();
    cards = new ArrayList<>(deck);
    Collections.shuffle(cards);
  }

  public Card removeTopmostCard()
  {
    return cards.remove(0);
  }
}
