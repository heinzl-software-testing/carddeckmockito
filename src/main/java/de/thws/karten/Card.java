package de.thws.karten;


public record Card(Suit suit, Rank rank)
{
}

//record ist gleichbedeutend mit folgender Klasse
//public class Card
// {
//  Suit suit;
//  Rank rank;
//
//  public Card(Suit suit, Rank rank)
//  {
//    this.suit = suit;
//    this.rank = rank;
//  }
//
//  @Override
//  public boolean equals(Object o)
//  {
//    if (this == o) return true;
//    if (o == null || getClass() != o.getClass()) return false;
//    Card card = (Card) o;
//    return suit == card.suit && rank == card.rank;
//  }
//
//  @Override
//  public int hashCode()
//  {
//    return Objects.hash(suit, rank);
//  }
//}
