package de.thws.karten;

import org.junit.jupiter.api.Test;

import static de.thws.karten.Game.NUMBER_OF_CARDS_FOR_EACH_PLAYERS_STARTING_HAND;
import static org.mockito.Mockito.*;

import static org.junit.jupiter.api.Assertions.*;

public class DeckTest
{
  @Test
  void testDeckCreation()
  {
    Deck deck = new Deck(); //Arrange

    deck.reset();           //Act

    assertTrue(deck.cards.contains(new Card(Suit.CLUBS, Rank.JACK)));  //Assert
    assertTrue(deck.cards.contains(new Card(Suit.DIAMONDS, Rank.EIGHT)));
    assertTrue(deck.cards.contains(new Card(Suit.HEARTS, Rank.SEVEN)));
    assertTrue(deck.cards.contains(new Card(Suit.SPADES, Rank.ACE)));
  }

  @Test
  void testPlayerCreation()
  {
    Deck deck = new Deck();            //Arrange
    deck.reset();
    int deckSize = deck.cards.size();

    Player player = new Player();
    player.drawStartingHand(deck);      //Act

    assertEquals(NUMBER_OF_CARDS_FOR_EACH_PLAYERS_STARTING_HAND, player.hand.size()); //Assert
    assertTrue(deck.cards.size() == deckSize - NUMBER_OF_CARDS_FOR_EACH_PLAYERS_STARTING_HAND);
  }

  @Test
  void testPlayCreation()
  {
    Game game = new Game().addPlayer(new Player())
        .addPlayer(new Player());

    game.createGame();

    assertEquals(NUMBER_OF_CARDS_FOR_EACH_PLAYERS_STARTING_HAND, game.players.get(0).hand.size());
    assertEquals(NUMBER_OF_CARDS_FOR_EACH_PLAYERS_STARTING_HAND, game.players.get(1).hand.size());
  }

  @Test
  void testPlayCreationWithMockito()
  {
    Deck deck = mock();
    Game game = new Game(deck).addPlayer(new Player())
        .addPlayer(new Player());
    when(deck.removeTopmostCard()).thenReturn(new Card(Suit.HEARTS, Rank.ACE));

    game.createGame();

    assertTrue(game.running);
    verify(deck, times(2 * NUMBER_OF_CARDS_FOR_EACH_PLAYERS_STARTING_HAND)).removeTopmostCard();
    assertEquals(NUMBER_OF_CARDS_FOR_EACH_PLAYERS_STARTING_HAND, game.players.get(0).hand.size());
    assertEquals(NUMBER_OF_CARDS_FOR_EACH_PLAYERS_STARTING_HAND, game.players.get(1).hand.size());
  }
}
